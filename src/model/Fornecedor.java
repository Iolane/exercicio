package model;

import java.util.ArrayList;

public class Fornecedor {

	private String nome;
	private ArrayList<String> telefone;
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public ArrayList<String> getTelefone() {
		return telefone;
	}
	public void setTelefone(ArrayList<String> telefone) {
		this.telefone = telefone;
	}
	public Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	public ArrayList<Produto> getProdutos() {
		return produtos;
	}
	public void setProdutos(ArrayList<Produto> produtos) {
		this.produtos = produtos;
	}
	private Endereco endereco;
	private ArrayList<Produto> produtos;
}
